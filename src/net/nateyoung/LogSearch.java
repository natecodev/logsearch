package net.nateyoung;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LogSearch {

    public static void main(String[] args) {

        LogSearch logSearch = new LogSearch();

        File outputFile;

        File logFolder;
        List<File> logs = new ArrayList<File>();
        String query = "";

        logFolder = logSearch.getLogFolder();
        outputFile = logSearch.getOutputFile();
        logs = logSearch.getLogs(logFolder);
        query = logSearch.getQuery();

        try {
            logSearch.findResults(logs, query, outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File getLogFolder() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(chooser.DIRECTORIES_ONLY);

        return getFile(chooser);
    }

    public File getOutputFile() {
        return new File("output_"+UUID.randomUUID()+".txt");
    }

    public File getFile(JFileChooser chooser) {
        if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }

        return null;
    }

    public List<File> getLogs(File logFolder) {
        List<File> logs = new ArrayList<File>();

        for (File file : logFolder.listFiles()) {
            logs.add(file);
        }

        return logs;
    }

    public String getQuery() {
        return JOptionPane.showInputDialog("Query:");
    }

    public void findResults(List<File> logs, String query, File outputFile) throws IOException {
        PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

        writer.flush();

        for (File log : logs) {
            BufferedReader br = new BufferedReader(new FileReader(log));

            writer.println("File: " + log.getAbsolutePath());

            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains(query)) {
                    writer.println(line);
                }
            }
        }

        writer.close();
    }

}
